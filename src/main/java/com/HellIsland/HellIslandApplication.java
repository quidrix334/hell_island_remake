package com.HellIsland;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class HellIslandApplication {

	public static void main(String[] args) {
		SpringApplication.run(HellIslandApplication.class, args);
	}

}
